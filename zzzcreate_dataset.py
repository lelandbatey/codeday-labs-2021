# This file is used to generate many of the datasets in the
# simple_weighted_lottery_algorithm.py file. But it's a mess and is only here
# so I can come back and rebuild more data in the
# simple_weighted_lottery_algorithm.py in the future. Please pay no attention
# to this file.

from simple_weighted_lottery_algorithm import candidates, previous_occurance_winners

import random

# for _ in range(4):
# print(random.sample(candidates, 2))

name_count = dict()
for occur in previous_occurance_winners:
    for w in occur:
        name = w['name']
        count = name_count.get(name, 0)
        name_count[name] = count + 1

bigkeys = sorted(name_count.keys(), key=lambda k: name_count[k])
for k in bigkeys[::-1]:
    print(f"{k:10} {name_count[k]:3}")

# print(name_count)

tickets_for_candidates = dict()
max_attendences = max([name_count[x] for x in name_count])
print(max_attendences)
for cand in candidates:
    name = cand['name']
    tickets_for_candidates[name] = max_attendences - name_count[name] + 1
for k in sorted(
    sorted(tickets_for_candidates.keys())[::-1], key=lambda k: tickets_for_candidates[k]
)[::-1]:
    print(f"{k:10} {tickets_for_candidates[k]:3}")
print(tickets_for_candidates)

from fractions import Fraction
chances = dict()
total_tickets = sum(tickets_for_candidates.values())
for k in sorted(
    sorted(tickets_for_candidates.keys())[::-1], key=lambda k: tickets_for_candidates[k]
)[::-1]:
    fract = Fraction(tickets_for_candidates[k], total_tickets)
    pct = f'{100*(fract.numerator/fract.denominator):.1f}%'
    print(f"{k:10} {fract.numerator:>3}/{fract.denominator:<3} {pct}")
