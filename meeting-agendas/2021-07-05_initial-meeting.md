# Event Lottery Meeting Agenda - 2021-07-05

- Get to know your students (background, interests in CS, and more)
- Present your vision for the project, and get student feedback
- Figure out ground level technicals: VCS, task management tool, communication, etc
- Agree on a project scope: what will/won't your students do
- Set guidelines for task management and check-ins (scrum, etc)
- Determine what background students are entering with, and who needs what help


## Get to Know students
- I'm a senior developer at Glympse
- Ten years ago SRND
- CS degree WSU
- Golang, Truss, SQL, Python, MongoDB, AWS


- Abenezer, please introduce yourself
    - What're your interests in technology?
    - What classes/projects have you been most interested in so far?
    - Why did you choose this project? What do you want to learn from this project?
- William, please introduce yourself
    - What're your interests in technology?
    - What classes/projects have you been most interested in so far?
    - Why did you choose this project? What do you want to learn from this project?


## Vision of the project, feedback

The vision I have so far is to create a service & web application for weighted
lottery based event registration. It allows you to create events and invite
friends, but the service holds a lottery in order to determine who receives a
formal invitation. Useful for cases where you have reoccurring IRL events with
limited capacity, and where you want people who haven't had the chance to
attend to be less likely to get chosen than those who've attended before. Since
I have no idea what y'all are capable of, I have four "tiers" of difficulty as
far as architectural complexity and features. I figure we start with "tier #1"
and move up from there as we complete functionality. The tiers are:

- Tier 1: Minimum event creation and lottery
    - Minimum interactivity
    - Events are singular
    - Events are create-only, no modification
    - All events are visible to all users, no access control (beyond URL obscurity)
    - Users can register to be invited by providing a phone number and their name
    - Users are uniformly randomly selected from those who signed up, no weighting
    - Winners are notified by a single SMS to their phone number
    - Options for implementation:
        - How fancy do you want the UI to be?
        - What "thing" causes the lottery to happen? Automatic at X time before event starts? Or a mouse click on a button?
            - Automatic more challenging as requires scheduling
            - Mouse click requires some kind of password protection (or other way of allowing only the "right" person to trigger the lottery to be held)
- Tier 2: Adding more entities so features match what people expect more closely
    - Add ability for users to also create "accounts" w/ username and passwords
    - Add new thing call "Occurance" which is instance of an event
    - Make events be like a template for an "Occurance"
    - Allow users to own events (and thus also to own the "Occurance"s of those Events)
    - Allow users to modify events and occurances (e.g. allow rescheduling of occurances or changing descriptions, titles, etc)
    - Options for implementation:
        - How fancy do you want the UI to be?
        - How do you want to do auth? Cookie based sessions? Custom auth token in an HTTP header?
- Tier 3: Winners can confirm invitation via SMS and decline via SMS
    - Two-way SMS via Twilio in order for users to "CONFIRM" or "DECLINE" an invitation
    - If users don't accept the invite within a certain amount of time, they automatically are said to have "declined" the invite and a new lottery is held to invite another as-yet uninvited person.
    - Have to further modify the DB schema so winners can have this confirmed/declined states
    - Options for implementation:
        - Do you want users to be reminded that they've been invited but they haven't confirmed?
        - Should the system "lock in" the potential winners at a particular time? If so that's more work
- Tier 4: Weighting, actual attendance, and penalties
    - Add UI for event owners to mark invitees as "actually attended" or "didn't attend"
    - Implement algorithm so we can weight users based on amount of times invited to previous occurances, or times where users confirmed their invite but then didn't attend.
    - Options for implementations:
        - Many choices to be made about how to weight different observable things
        - Do we add UI to allow occurance owner to manually change weights for users?

Questions for the students:

- Who will use this project?

## Ground-level technicals

- I vote that we use GitLab
- Suggest we use DigitalOcean droplets for hosting

## Agree on a project scope: will do, won't do

- Not doing
    - No mobile app
- Yes doing
    - SQL database?
    - Golang based backend?
    - Simple frontend? SPA? Fancy JS/React?

## Set guidelines for task management and check-ins

My preferred guidelines for task management and checks-ins are in the following
list. In general, a rough SCRUM-like experience is the way to go for small
teams (in my experience).

- Twice weekly (potentially more) video calls
    - Start of week: What are we planning?
    - End of week: What have we done, and what's next?
- Task management handled in Trello
    - Students should write the tasks, though I can provide guidance and write-along if need be
    - For first round, I can write tasks with the students


## Determine what background students are entering with, and who needs what help

Both my students have been coding for at least a year. And it seems there's
some experience with JS on the frontend as well. I have some questions
remaining about what the students can/should do.


