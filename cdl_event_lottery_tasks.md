
# Tasks for EL

This document contains the draft-versions of user stories and the tasks which
derive from those user stories. These stories and tasks are broken down by the
four sections/versions of the application.


# v1 - Event create & phone\#-based registration

I don't know how much of this I should delegate to the students and how much I
should be proscriptive about. So to start, I'm going to be ultra proscriptive
and have a full set of stories and tasks which these students should complete.
The stories are written in the ["Given, When, Then" style.](https://en.wikipedia.org/wiki/Given-When-Then)

## Stories

### S1.1 Create event

```
As a user (any person with a web browser)
Given that I visit the URL for creating an event
And I can view a form to fill out describing the event to create
When I click the "submit" button on that form
Then I should be taken to a new page where I am viewing the newly created event
```

### S1.2 Register for an event
```
As a user (person w/ web browser)
Given that I visit the page of an Event
And I see a form saying "Register" where I can add my Name and Phone number
When I click the "register" button
Then I should be listed as a candidate on the event page
```

### S1.3 Lottery is held based on time

```
As an event organizer
Given that 3 people have registered and my event will only select 1 winner
When we're 24 hours before my event
Then 1 candidate will be selected as a winner and will recieve an SMS invitation
```

