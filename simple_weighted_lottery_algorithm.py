# The candidates for the current event
candidates = [{
    'name': 'Alice',
    'phonenum': '+12235558501'
}, {
    'name': 'Bob',
    'phonenum': '+11525554026'
}, {
    'name': 'Carol',
    'phonenum': '+15525557775'
}, {
    'name': 'Dan',
    'phonenum': '+14355559022'
}, {
    'name': 'Erin',
    'phonenum': '+11535551885'
}]

previous_occurence_winners = [[
    {
        'name': 'Carol',
        'phonenum': '+15525557775'
    },
    {
        'name': 'Erin',
        'phonenum': '+11535551885'
    },
], [
    {
        'name': 'Alice',
        'phonenum': '+12235558501'
    },
    {
        'name': 'Bob',
        'phonenum': '+11525554026'
    },
], [
    {
        'name': 'Erin',
        'phonenum': '+11535551885'
    },
    {
        'name': 'Bob',
        'phonenum': '+11525554026'
    },
], [
    {
        'name': 'Dan',
        'phonenum': '+14355559022'
    },
    {
        'name': 'Bob',
        'phonenum': '+11525554026'
    },
]]

# The count of times that each person was able to attend an occurrence of the
# event. Calculated from the previous_occurence_winners data.
attendence_counts = {
    'Bob': 3,
    'Erin': 2,
    'Alice': 1,
    'Carol': 1,
    'Dan': 1,
}

# The number tickets to give each person in the upcoming raffle. Implementation
# for how to calculate this is left as an exercise to the reader, though the
# math is like so:
#     tickets_for_one_person = (most times any one person's attended) - (times this user has attended) + 1
tickets_per_person = {
    'Alice': 3,
    'Carol': 3,
    'Dan': 3,
    'Erin': 2,
    'Bob': 1,
}

import random


def remove_all(lst, val):
    '''remove_all removes all values of 'val' from list 'lst'. A helper
    function for raffle() '''
    new = list()
    for x in lst:
        if x != val: new.append(x)
    return new


def raffle(tickets_per_person, unique_winners=2):
    '''raffle simulates a raffle and returns 'unique_winners' users.'''
    ticket_hopper = list()
    for person, ticket_count in tickets_per_person.items():
        for _ in range(ticket_count):
            ticket_hopper.append(person)
    winners = list()
    for _ in range(unique_winners):
        winner = random.choice(ticket_hopper)
        winners.append(winner)
        # So that this for loop can run a fixed number of times
        ticket_hopper = remove_all(ticket_hopper, winner)
    return winners


# Now run a raffle and see who our two winners are!
#
# Here are the chances that each person will win the raffle, if they were in a
# raffle with only one winner:
#     Alice        1/4   25.0%
#     Carol        1/4   25.0%
#     Dan          1/4   25.0%
#     Erin         1/6   16.7%
#     Bob          1/12  8.3%
print(raffle(tickets_per_person))
