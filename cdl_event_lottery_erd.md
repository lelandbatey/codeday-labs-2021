# What is this project?
This document outlines progressively more complex architectures for the 2021
CodeDay Labs weighted lottery event system. The goal is to create a service &
web application for weighted lottery based event registration. It allows you to
create events and invite friends, but the service holds a lottery in order to
determine who receives a formal invitation. Useful for cases where you have
reoccurring IRL events with limited capacity, and where you want people who
haven't had the chance to attend to be less likely to get chosen than those
who've attended before.

Since we don't know what you the students are capable of, this document starts
with a very simple version of the service with very few features, then adds
complexity and features with each new version. Don't be fooled though; no
matter what level of architecture we the team choose to aim for, you can build
an amazingly effective and simple to use event registration system at any of
these architecture levels.

# v1 - Event create & phone\#-based registration
This version is meant to be the minimum possible version of the service, with
lots of constraints and the simplest possible setup. Restraints like: there
won't be users, anyone can create an event, once an event is created you won't
be able to edit the event, people will only be able to register for the event
via their phone number/name combo. For now we want the lottery process to be a
simple unweighted random selection from the candidates. We can flex as much as
we want, but the goal is to implement the _simplest possible_ website which
does something like what we want. The aim's to make this simple, so I'd
recommend using something incredibly self-contained like SQLite for our DB, not
even needing to use Postgres for the time being.

```plantuml
@startuml
skinparam dpi 192
left to right direction

entity Event {
    * ID integer
    --
    * LotteryTime  Datetime
    * CandidatesBlob JSONBlob
    * MaxAttendees integer
    * Start Datetime
    * End Datetime
}

entity Winner {
    * ID integer
    * PhoneNumber string
    * EventID <<FK>>
}

Winner }o--|| Event
@enduml
```
```plantuml
@startuml
skinparam dpi 192
actor bob as "Bob"
actor alice as "Alice"
participant fe as "Front\nEnd"
participant sch as "Scheduler"
participant svc as "Backend"
database db as "Database"
participant sms as "SMS Sender"

' Three sequences we care about for this stage:
' 1. Create an event
' 2. Register for an event
' 3. Conduct lottery for the event

group Create an event

alice -> fe : Alice has filled out the event\ncreation form and clicks submit
fe -> svc : Frontend code sends data to backend\n""POST /api/v1/event""\n""{"name": "...", "start": "...", "end": "..."}""
svc -> db : Backend makes SQL request to create event\n""INSERT into events (name, start, end)""\n""   VALUES("...", "...", "...")""
db -> svc
svc -> fe
fe -> alice : Page changes to\nthe "event display page"

end

group Register for an event

bob -> fe : Bob has filled out registration\nform and clicks submit
fe -> svc : ""POST /api/v1/event/register""\n""{"name": "Bob", "phonenum": "+11525554026"}
svc -> db : ""SELECT candidatesblob FROM events""\n""  WHERE events.ID = 42;""
db -> svc : DB returns the candidatesblob of JSON\n to the backend
svc -> db : Backend updates the candidatesblob\n with info about Bob\n""UPDATE events SET candidatesblob = ...""\n""WHERE ID = 42;""
db -> svc
svc -> fe
fe -> bob

end

group Conduct lottery for the event
sch -> svc : Scheduler tells service\nto conduct the lottery
note right
Technically, this could be triggered by
a scheduler, but it could also be
triggered in many other ways. For
example, it could be triggered by a
password based request to the backend
(though that would require storing a
per-event password).
end note
svc -> db : Get's event and candidates\n""SELECT ... FROM events WHERE ID = 42;""
db -> svc
svc -> db : Update event candidates, marking which\ncandidates won the lottery and are\nrecieving invites.
svc -> sms
sms -> bob : Tell the winners they won\nvia SMS


end


@enduml
```

# v2 - Add Users and Reoccurring events

This version is where we take what we learned with the basic service and we
start to add concepts like a "user" with a password who can log in and create
events, and the separation of an event as an idea vs a specific *occurrence*
of an event. However, we're still going to try to keep things simple for our
users, so registering for an event won't require any form of account; all a
user needs to do is provide their phone # and a name to designate them.
Likewise, we're still keeping the candidate selection as a simple unweighted
random sample from the candidates.

## Event as idea vs Occurrence of event

Let's say that you and your friends decide to get together on a Thursday night
and watch a movie. You watch the movie and you decide you want to do this more
often; you all decide you should start a "Movie night", and you'll meet on
Thursdays.

What's just happened is that you've come up with the idea for an event, called
"Movie night", and you all have tacitly scheduled *occurrences* of that event in
the future. Having these as separate but related things is useful in our
service, since it allows us to more closely model what's happening in the "real
world". Let's consider some examples:

### Cancellation
> This month we have movie night scheduled for every Thursday. However,
> everyone will be going to the big festival on week 3 of this month, so we
> won't be able to hold movie night. So we're canceling that occurrence of movie
> night, and we'll re-convene for our regularly scheduled movie night on
> Thursday of week 4 of this month.

Here, we don't want to mix the concept of canceling "movie night" with the idea
of canceling a _single occurrence_ of movie night. By making them separate but
related things in our DB, we can allow them to change separately in the ways
the real-world requires.

### Rescheduling
> We're holding movie night on Thursday, but Alice, the host of movie night,
> has to visit her grandmother in the hospital that day. Because Alice can't
> hold movie night that day, she's rescheduled movie night to be Friday of this
> week.

Once again we see that in the "real world", a reoccurring event and an
occurrence of that event are separate things. Since we want our system to model
the real world, we make them separate in our DB as well.

```plantuml
@startuml
left to right direction
skinparam dpi 192

entity User {
    * ID integer
    --
    * Username string
    * Email string
    * Password bytes
}

entity Event {
    * ID integer
    --
    * Name string
    * Description string
    * Location string
    * Owner <<FK>>
}

entity Occurance {
    * ID integer
    --
    * EventID <<FK>>
    * LotteryTime  Datetime
    * CandidatesBlob JSONBlob
    * MaxAttendees integer
    * Start Datetime
    * End Datetime
    * Description string
    * Location string
    * Name string
}

entity Winner {
    * ID integer
    --
    * PhoneNumber string
    * OccuranceID <<FK>>
}

Winner }o--|| Event
Winner }o--|| Occurance
Occurance }o--|| Event
Event }o--|| User

@enduml
```

# v3 - Winners can confirm attendance via SMS and decline via SMS

This step introduces the ability for winners to confirm or decline they'll be
able to actually attend the occurrence, before that occurrence happens.
Additionally, we will require users to confirm that they'll attend within X
time of winning the lottery, and if the winner doesn't confirm then they are
considered to have declined.

We use these "confirm/decline" rules to allow the service to invite more
candidates who haven't yet won the lottery, with the hope being that the number
of people who _could_ attend and the number of people who _actually do_ attend
are the same.

```plantuml
@startuml
left to right direction
skinparam dpi 192

entity User {
    * ID integer
    --
    * Username string
    * Email string
    * Password bytes
}

entity Event {
    * ID integer
    --
    * Name string
    * Description string
    * Location string
    * Owner <<FK>>
}

entity Occurance {
    * ID integer
    --
    * EventID <<FK>>
    * LotteryTime  Datetime
    * CandidatesBlob JSONBlob
    * MaxAttendees integer
    * Start Datetime
    * End Datetime
    * Description string
    * Location string
    * Name string
}

entity Winner {
    * ID integer
    --
    * PhoneNumber string
    * OccuranceID <<FK>>
    * CreateTime Datetime
    * ExpireTime Datetime
    * DeclineTime DateTime
    * AcceptTime DateTime
}

Winner }o--|| Event
Winner }o--|| Occurance
Occurance }o--|| Event
Event }o--|| User

@enduml
```

# v4 - Weighting, actual attendance, and penalties

In this final level, we get to the "weighted" part of the phrase "weighted
lottery based event registration". Now that we have this connection between
events, occurrences of events, and winners for each event, we can use that data
to implement a weighted lottery. One simple algorithm for a weighted lottery is
to simulate a raffle, with the users who've already gone the most times getting
1 ticket, and everyone else getting a number of tickets equal to
`(most times any one person's attended) - (times this user has attended) + 1`.
For an example of this kind of lottery, see the code in
`simple_weighted_lottery_algorithm.py` in this snippet.

At the same time as this, we want to add penalties to users who have misbehaved
in the past. For example, someone who won the lottery, said they were going to
attend, but then didn't actually attend. In cases like this, we as the event
organizers may want to forever reduce their chances of being invited back. In
order to penalize for not attending though, our data model must become aware of
"attendees". Thus, we add the `Attendee` entity. Note that the exact "how" of
implementing penalties for absenteeism is up to the team; maybe it's a harsh
penalty, maybe a lenient one, it's up to the team.


```plantuml
@startuml
left to right direction
skinparam dpi 192

entity User {
    * ID integer
    --
    * Username string
    * Email string
    * Password bytes
}

entity Event {
    * ID integer
    --
    * Name string
    * Description string
    * Location string
    * Owner <<FK>>
}

entity Occurance {
    * ID integer
    --
    * EventID <<FK>>
    * LotteryTime  Datetime
    * CandidatesBlob JSONBlob
    * MaxAttendees integer
    * Start Datetime
    * End Datetime
    * Description string
    * Location string
    * Name string
}

entity Winner {
    * ID integer
    --
    * PhoneNumber string
    * OccuranceID <<FK>>
    * CreateTime Datetime
    * ExpireTime Datetime
    * DeclineTime DateTime
    * AcceptTime DateTime
}

entity Attendee {
    * ID integer
    --
    * PhoneNumber string
    * OccurenceID <<FK>>
}

Winner }o--|| Event
Winner }o--|| Occurance
Occurance }o--|| Event
Event }o--|| User
Occurance }o--|| Attendee
Attendee |o--|| Winner

@enduml
```

